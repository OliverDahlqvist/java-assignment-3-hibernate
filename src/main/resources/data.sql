INSERT INTO character (character_name, character_alias, character_gender, character_picture)
VALUES ('Legolas', 'Elf', 'male', 'https://static.wikia.nocookie.net/lotr/images/3/33/Legolas_-_in_Two_Towers.PNG/revision/latest?cb=20120916035151');
INSERT INTO character (character_name, character_alias, character_gender, character_picture)
VALUES ('Gandalf', 'Grey Pilgrim', 'male', 'https://static.wikia.nocookie.net/lotr/images/e/e7/Gandalf_the_Grey.jpg/revision/latest?cb=20121110131754');
INSERT INTO character (character_name, character_alias, character_gender, character_picture)
VALUES ('Frodo Baggins', 'Ring-bearer', 'male', 'https://fantasytopics.com/wp-content/uploads/2022/03/Scene-wheer-Frodo-leaves-Middle-Earth.jpeg.webp');
INSERT INTO character (character_name, character_alias, character_gender, character_picture)
VALUES ('Aragorn', 'Eagle of the Star', 'male', 'https://static.wikia.nocookie.net/lotr/images/b/b6/Aragorn_profile.jpg/revision/latest?cb=20170121121423');
INSERT INTO character (character_name, character_alias, character_gender, character_picture)
VALUES ('Galadriel', 'Lady of the woods of Lothlorien', 'female', 'https://static.wikia.nocookie.net/lotr/images/c/cb/Galadriel.jpg/revision/latest?cb=20151015204512');

INSERT INTO movie (movie_director, movie_genre, movie_release_year, movie_title, movie_trailer)
VALUES ('Joe Russo', 'Action', '2022-07-22', 'The Gray Man', 'https://www.youtube.com/watch?v=BmllggGO4pM');
INSERT INTO movie (movie_director, movie_genre, movie_release_year, movie_title, movie_trailer)
VALUES ('Peter Jackson', 'Action', '2001-03-02', 'The Lord of the Rings: The Fellowship of the Ring', 'https://www.youtube.com/watch?v=BmllggGO4pM');
INSERT INTO movie (movie_director, movie_genre, movie_release_year, movie_title, movie_trailer)
VALUES ('Peter Jackson', 'Action', '2002-03-02', 'The Lord of the Rings: The Two Towers', 'https://www.youtube.com/watch?v=BmllggGO4pM');
INSERT INTO movie (movie_director, movie_genre, movie_release_year, movie_title, movie_trailer)
VALUES ('Peter Jackson', 'Action', '2003-03-02', 'The Lord of the Rings: The Return of the King', 'https://www.youtube.com/watch?v=BmllggGO4pM');


INSERT INTO franchise (franchise_description, franchise_name)
VALUES ('Hallo där blåbär', 'Lord of the Rings Franchise');

insert into movie_character (movie_id, character_id) VALUES (1, 1);
insert into movie_character (movie_id, character_id) VALUES (1, 2);
insert into movie_character (movie_id, character_id) VALUES (1, 3);
insert into movie_character (movie_id, character_id) VALUES (1, 4);
insert into movie_character (movie_id, character_id) VALUES (1, 5);
insert into movie_character (movie_id, character_id) VALUES (2, 1);
insert into movie_character (movie_id, character_id) VALUES (2, 2);
insert into movie_character (movie_id, character_id) VALUES (2, 3);
insert into movie_character (movie_id, character_id) VALUES (2, 4);
insert into movie_character (movie_id, character_id) VALUES (2, 5);
insert into movie_character (movie_id, character_id) VALUES (3, 1);
insert into movie_character (movie_id, character_id) VALUES (3, 2);
insert into movie_character (movie_id, character_id) VALUES (3, 3);
insert into movie_character (movie_id, character_id) VALUES (3, 4);
insert into movie_character (movie_id, character_id) VALUES (3, 5);

insert into franchise (franchise_name, franchise_description) values ('The Lord of the Rings', 'The Lord of the Rings is a series of three epic fantasy adventure films directed by Peter Jackson, based on the novel written by J. R. R. Tolkien. The films are subtitled The Fellowship of the Ring, The Two Towers, and The Return of the King.');

UPDATE movie SET franchise_id = 2 WHERE movie_title LIKE '%Lord of the Rings%';

