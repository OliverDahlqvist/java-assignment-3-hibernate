package com.experis.moviesystem.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

/**
 * Character model.
 */
@Entity
@Getter
@Setter
public class Character {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "character_id")
    private int id;

    @Column(name = "character_name")
    private String name;

    @Column(name = "character_alias")
    private String alias;

    @Column(name = "character_gender")
    private String gender;

    @Column(name = "character_picture")
    private String picture;

    @ManyToMany(mappedBy = "characters")
    private Set<Movie> movies;
}
