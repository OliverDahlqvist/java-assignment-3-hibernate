package com.experis.moviesystem.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

/**
 * Franchise model.
 */
@Entity
@Setter
@Getter
public class Franchise {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "franchise_id")
    private int id;

    @Column(name = "franchise_name")
    private String name;

    @Column(name = "franchise_description")
    private String description;

    @OneToMany(mappedBy = "franchise")
    private Set<Movie> movies;
}
