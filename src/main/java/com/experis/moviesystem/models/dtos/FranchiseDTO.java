package com.experis.moviesystem.models.dtos;

import com.experis.moviesystem.models.Movie;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.OneToMany;
import java.util.Set;

/**
 * Franchise DTO.
 */
@Data
public class FranchiseDTO {
    private int id;
    private String name;
    private String description;
    private Set<Integer> movies;
}
