package com.experis.moviesystem.models.dtos;

import com.experis.moviesystem.models.Character;
import com.experis.moviesystem.models.Franchise;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * Movie DTO.
 */
@Data
public class MovieDTO {
    private int id;
    private String title;
    private String genre;
    private Date releaseYear;
    private String director;
    private String picture;
    private String trailer;
    private Set<Integer> characters;
    private Integer franchise;
}
