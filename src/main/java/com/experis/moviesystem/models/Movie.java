package com.experis.moviesystem.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Movie model.
 */
@Entity
@Getter
@Setter
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "movie_id")
    private int id;

    @Column(name = "movie_title")
    private String title;

    @Column(name = "movie_genre")
    private String genre;

    @Column(name = "movie_release_year")
    private Date releaseYear;

    @Column(name = "movie_director")
    private String director;

    @Column(name = "movie_picture")
    private String picture;

    @Column(name = "movie_trailer")
    private String trailer;

    @ManyToMany
    @JoinTable(name = "movie_character",
    joinColumns = {@JoinColumn(name = "movie_id")},
    inverseJoinColumns = {@JoinColumn(name = "character_id")})
    private Set<Character> characters;

    @ManyToOne
    @JoinColumn(name = "franchise_id")
    private Franchise franchise;
}
