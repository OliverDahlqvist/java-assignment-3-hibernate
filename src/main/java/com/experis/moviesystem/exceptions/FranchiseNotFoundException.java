package com.experis.moviesystem.exceptions;

/**
 * Used to throw runtime exceptions when franchises are not found in the db.
 */
public class FranchiseNotFoundException extends RuntimeException {
    public FranchiseNotFoundException(int id) {
        super("Franchise not found with ID: " + id);
    }
}
