package com.experis.moviesystem.exceptions;

/**
 * Used to throw runtime exceptions when characters are not found in the db.
 */
public class CharacterNotFoundException extends RuntimeException {
    public CharacterNotFoundException(int id) {
        super("Character not found with ID: " + id);
    }
}
