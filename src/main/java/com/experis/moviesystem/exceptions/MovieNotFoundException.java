package com.experis.moviesystem.exceptions;

/**
 * Used to throw runtime exceptions when movies are not found in the db.
 */
public class MovieNotFoundException extends RuntimeException {
    public MovieNotFoundException(int id) {
        super("Movie does not exist with ID: " + id);
    }
}
