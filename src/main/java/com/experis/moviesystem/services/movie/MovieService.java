package com.experis.moviesystem.services.movie;

import com.experis.moviesystem.models.Movie;
import com.experis.moviesystem.services.CrudService;

public interface MovieService extends CrudService<Movie, Integer> {
}
