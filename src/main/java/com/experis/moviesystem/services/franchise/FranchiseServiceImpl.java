package com.experis.moviesystem.services.franchise;

import com.experis.moviesystem.exceptions.FranchiseNotFoundException;
import com.experis.moviesystem.models.Franchise;
import com.experis.moviesystem.repositories.FranchiseRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

/**
 * Franchise services used by controllers.
 */
@Service
public class FranchiseServiceImpl implements FranchiseService {

    private final FranchiseRepository franchiseRepository;

    public FranchiseServiceImpl(FranchiseRepository franchiseRepository) {
        this.franchiseRepository = franchiseRepository;
    }

    @Override
    public Franchise findById(Integer id) {
        return franchiseRepository.findById(id).orElseThrow(() -> new FranchiseNotFoundException(id));
    }

    @Override
    public Collection<Franchise> findAll() {
        return franchiseRepository.findAll();
    }

    @Override
    public Franchise add(Franchise entity) {
        return franchiseRepository.save(entity);
    }

    @Override
    public Franchise update(Franchise entity) {
        return franchiseRepository.save(entity);
    }

    @Override
    public void deleteById(Integer id) {
        Optional<Franchise> franchise = franchiseRepository.findById(id);
        if(franchise.isEmpty()) return;
        franchise.get().getMovies().forEach(m -> m.setFranchise(null));
        franchiseRepository.deleteById(id);
    }
}
