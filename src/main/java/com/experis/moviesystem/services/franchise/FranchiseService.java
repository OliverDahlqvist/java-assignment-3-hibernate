package com.experis.moviesystem.services.franchise;

import com.experis.moviesystem.models.Franchise;
import com.experis.moviesystem.services.CrudService;

public interface FranchiseService extends CrudService<Franchise, Integer> {
}
