package com.experis.moviesystem.services.character;

import com.experis.moviesystem.exceptions.CharacterNotFoundException;
import com.experis.moviesystem.models.Character;
import com.experis.moviesystem.repositories.CharacterRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;

/**
 * Character services used by controllers.
 */
@Service
public class CharacterServiceImpl implements CharacterService {
    private final CharacterRepository characterRepository;

    public CharacterServiceImpl(CharacterRepository characterRepository) {
        this.characterRepository = characterRepository;
    }

    @Override
    public Character findById(Integer id) {
        return characterRepository.findById(id).orElseThrow(() -> new CharacterNotFoundException(id));
    }

    @Override
    public Collection<Character> findAll() {
        return characterRepository.findAll();
    }

    @Override
    public Character add(Character entity) {
        return characterRepository.save(entity);
    }

    @Override
    public Character update(Character entity) {
        return characterRepository.save(entity);
    }

    @Override
    public void deleteById(Integer id) {
        if(characterRepository.findById(id).isEmpty()) return;
        Character character = characterRepository.findById(id).get();
        character.getMovies().forEach(m -> m.getCharacters().remove(character));
        characterRepository.deleteById(id);
    }

    @Override
    public boolean exists(int id) {
        return characterRepository.existsById(id);
    }
}
