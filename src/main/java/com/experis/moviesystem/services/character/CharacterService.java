package com.experis.moviesystem.services.character;

import com.experis.moviesystem.models.Character;
import com.experis.moviesystem.services.CrudService;

public interface CharacterService extends CrudService<Character, Integer> {
    /**
     * Checks if a character exists with the given ID.
     * @param id ID of character to check if it exists.
     * @return True if character with given id exists.
     */
    boolean exists(int id);
}
