package com.experis.moviesystem.services.mission;

import com.experis.moviesystem.models.Mission;
import com.experis.moviesystem.services.CrudService;

public interface MissionService extends CrudService<Mission, Integer> {
}
