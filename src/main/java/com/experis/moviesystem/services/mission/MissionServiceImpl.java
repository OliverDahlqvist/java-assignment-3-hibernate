package com.experis.moviesystem.services.mission;

import com.experis.moviesystem.models.Mission;
import com.experis.moviesystem.repositories.MissionRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class MissionServiceImpl implements MissionService {
    private final MissionRepository missionRepository;

    public MissionServiceImpl(MissionRepository missionRepository) {
        this.missionRepository = missionRepository;
    }

    @Override
    public Mission findById(Integer integer) {
        return null;
    }

    @Override
    public Collection<Mission> findAll() {
        return missionRepository.findAll();
    }

    @Override
    public Mission add(Mission entity) {
        return null;
    }

    @Override
    public Mission update(Mission entity) {
        return null;
    }

    @Override
    public void deleteById(Integer integer) {

    }
}
