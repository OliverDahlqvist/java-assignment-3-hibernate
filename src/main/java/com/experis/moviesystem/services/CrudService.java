package com.experis.moviesystem.services;

import java.util.Collection;

public interface CrudService <T, ID> {
    /**
     * Finds an entity by its ID.
     * @param id ID to query for.
     * @return Found entity.
     */
    T findById(ID id);

    /**
     * Finds all entities with the given type.
     * @return Collection of found entities.
     */
    Collection<T> findAll();

    /**
     * Adds a given entity to the db.
     * @param entity Entity to add.
     * @return Added entity.
     */
    T add(T entity);

    /**
     * Updates a given entity.
     * @param entity Entity to update.
     * @return Updated entity.
     */
    T update(T entity);

    /**
     * Deletes an entity by its ID.
     * @param id ID of entity to delete.
     */
    void deleteById(ID id);
}
