package com.experis.moviesystem.mappers;

import com.experis.moviesystem.models.Character;
import com.experis.moviesystem.models.Franchise;
import com.experis.moviesystem.models.Movie;
import com.experis.moviesystem.models.dtos.CharacterDTO;
import com.experis.moviesystem.services.franchise.FranchiseService;
import com.experis.moviesystem.services.movie.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Used to convert characters from DTOs and vice versa.
 */
@Mapper(componentModel = "spring")
public abstract class CharacterMapper {
    @Autowired
    protected MovieService movieService;

    /**
     * Maps a character to a characterDTO.
     * @param character Character to map.
     * @return Mapped characterDTO.
     */
    @Mapping(target = "movies", source = "movies", qualifiedByName = "moviesToIds")
    public abstract CharacterDTO characterToCharacterDto(Character character);

    /**
     * Maps a collection of characters to a collection of characterDTOs.
     * @param characters Collection of characters to map.
     * @return Collection of mapped characterDTOs.
     */
    public abstract Collection<CharacterDTO> charactersToCharactersDto(Collection<Character> characters);

    @Mapping(target = "movies", source = "movies", qualifiedByName = "movieIdsToMovies")
    public abstract Character characterDtoToCharacter(CharacterDTO characterDTO);

    /**
     * Maps set of ids to movies
     * @param ids Set of IDs.
     * @return Set of mapped movies.
     */
    @Named("movieIdsToMovies")
    Set<Movie> mapIdToMovies(Set<Integer> ids){
        return ids.stream().map(i -> movieService.findById(i)).collect(Collectors.toSet());
    }

    /**
     * Maps set of movies to set of IDs.
     * @param source Set of movies to map.
     * @return Set of mapped IDs.
     */
    @Named("moviesToIds")
    Set<Integer> mapMoviesToIds(Set<Movie> source){
        if(source == null) return null;
        return source.stream().map(c -> c.getId()).collect(Collectors.toSet());
    }
}
