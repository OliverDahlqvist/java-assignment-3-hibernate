package com.experis.moviesystem.mappers;

import com.experis.moviesystem.models.Franchise;
import com.experis.moviesystem.models.Movie;
import com.experis.moviesystem.models.dtos.FranchiseDTO;
import com.experis.moviesystem.services.movie.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Used to convert franchises from DTOs and vice versa.
 */
@Mapper(componentModel = "spring")
public abstract class FranchiseMapper {
    @Autowired
    protected MovieService movieService;

    /**
     * Maps a franchise to a franchiseDTO.
     * @param franchise Franchise to map.
     * @return Mapped franchiseDTO.
     */
    @Mapping(target = "movies", source = "movies", qualifiedByName = "moviesToIds")
    public abstract FranchiseDTO franchiseToFranchiseDto(Franchise franchise);

    /**
     * Maps a franchiseDTO to a franchise.
     * @param franchise Franchise to map.
     * @return Mapped franchise.
     */
    @Mapping(target = "movies", source = "movies", qualifiedByName = "movieIdsToMovies")
    public abstract Franchise franchiseDtoToFranchise(FranchiseDTO franchise);

    /**
     * Maps a collection of franchises to a collection of franchiseDTOs.
     * @param franchises Collection of franchises to map.
     * @return Collection of mapped franchiseDTOs.
     */
    public abstract Collection<FranchiseDTO> franchisesToFranchisesDto(Collection<Franchise> franchises);

    /**
     * Maps set of ids to movies
     * @param ids Set of IDs.
     * @return Set of mapped movies.
     */
    @Named("movieIdsToMovies")
    Set<Movie> mapIdToMovies(Set<Integer> ids){
        return ids.stream().map(i -> movieService.findById(i)).collect(Collectors.toSet());
    }

    /**
     * Maps set of movies to set of IDs.
     * @param source Set of movies to map.
     * @return Set of mapped IDs.
     */
    @Named("moviesToIds")
    Set<Integer> mapMoviesToIds(Set<Movie> source){
        if(source == null) return null;
        return source.stream().map(m -> m.getId()).collect(Collectors.toSet());
    }
}
