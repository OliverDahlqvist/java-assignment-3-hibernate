package com.experis.moviesystem.mappers;

import com.experis.moviesystem.models.Character;
import com.experis.moviesystem.models.Franchise;
import com.experis.moviesystem.models.Movie;
import com.experis.moviesystem.models.dtos.MovieDTO;
import com.experis.moviesystem.services.character.CharacterService;
import com.experis.moviesystem.services.franchise.FranchiseService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Used to convert movies from DTOs and vice versa.
 */
@Mapper(componentModel = "spring")
public abstract class MovieMapper {
    @Autowired
    protected CharacterService characterService;
    @Autowired
    protected FranchiseService franchiseService;

    @Mapping(target = "franchise", source = "franchise.id")
    @Mapping(target = "characters", source = "characters", qualifiedByName = "charactersToIds")
    public abstract MovieDTO movieToMovieDto(Movie movie);

    /**
     * Maps a collection of movies to movieDTOs.
     * @param movies Collection of movies to map.
     * @return Mapped collection of MovieDTOs.
     */
    public abstract Collection<MovieDTO> moviesToMoviesDto(Collection<Movie> movies);

    /**
     * Maps a movieDTO to movie.
     * @param movieDTO MovieDTO to map.
     * @return Mapped movie.
     */
    @Mapping(target = "franchise", source = "franchise", qualifiedByName = "franchiseIdToFranchise")
    @Mapping(target = "characters", source = "characters", qualifiedByName = "characterIdsToCharacters")
    public abstract Movie movieDtoToMovie(MovieDTO movieDTO);

    /**
     * Maps an ID to a franchise.
     * @param id ID of the franchise.
     * @return Found franchise with specified ID.
     */
    @Named("franchiseIdToFranchise")
    Franchise mapIdToFranchise(int id){
        return franchiseService.findById(id);
    }

    /**
     * Map a collection of IDs to a collection of characters.
     * @param ids Set of IDs to map.
     * @return Mapped set of characters.
     */
    @Named("characterIdsToCharacters")
    Set<Character> mapIdToCharacters(Set<Integer> ids){
        return ids.stream().map(i -> characterService.findById(i)).collect(Collectors.toSet());
    }

    /**
     * Map set of characters to their respective IDs.
     * @param source Set of characters to map.
     * @return Set of integers based on the mapped character ids.
     */
    @Named("charactersToIds")
    Set<Integer> mapCharactersToIds(Set<Character> source){
        if(source == null) return null;
        return source.stream().map(c -> c.getId()).collect(Collectors.toSet());
    }
}
