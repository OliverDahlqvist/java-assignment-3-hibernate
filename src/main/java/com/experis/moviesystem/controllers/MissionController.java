package com.experis.moviesystem.controllers;

import com.experis.moviesystem.models.Mission;
import com.experis.moviesystem.services.mission.MissionService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequestMapping(path = "api/v1/missions")
public class MissionController {
    private final MissionService missionService;

    public MissionController(MissionService missionService) {
        this.missionService = missionService;
    }

    @GetMapping
    public ResponseEntity<Collection<Mission>> findAll(){
        return ResponseEntity.ok(missionService.findAll());
    }
}
