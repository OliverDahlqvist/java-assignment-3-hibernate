package com.experis.moviesystem.controllers;

import com.experis.moviesystem.mappers.FranchiseMapper;
import com.experis.moviesystem.models.Franchise;
import com.experis.moviesystem.models.dtos.FranchiseDTO;
import com.experis.moviesystem.services.franchise.FranchiseService;
import com.experis.moviesystem.util.ApiErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

@RestController
@RequestMapping(path = "api/v1/franchises")
public class FranchiseController {
    private final FranchiseService franchiseService;
    private final FranchiseMapper franchiseMapper;

    public FranchiseController(FranchiseService franchiseService, FranchiseMapper franchiseMapper) {
        this.franchiseService = franchiseService;
        this.franchiseMapper = franchiseMapper;
    }

    /**
     * Gets franchise with specified ID.
     * @param id Franchise ID to search for.
     * @return Ok response.
     */
    @Operation(summary = "Get a franchise by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseDTO.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Franchise does not exist with supplied ID",
                    content = { @Content( mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @GetMapping(value = "/{id}")
    public ResponseEntity<FranchiseDTO> findById(@PathVariable int id){
        FranchiseDTO franchiseDTO = franchiseMapper.franchiseToFranchiseDto(franchiseService.findById(id));
        return ResponseEntity.ok(franchiseDTO);
    }

    /**
     * Gets all franchises from the database.
     * @return Ok response.
     */
    @Operation(summary = "Get all franchises")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = FranchiseDTO.class))) }),
            @ApiResponse(responseCode = "404",
                    description = "Franchise does not exist with supplied ID",
                    content = { @Content( mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @GetMapping
    public ResponseEntity<Collection<FranchiseDTO>> findAll(){
        Collection<FranchiseDTO> franchiseDTOS = franchiseMapper.franchisesToFranchisesDto(franchiseService.findAll());
        return ResponseEntity.ok(franchiseDTOS);
    }

    /**
     * Post operation which adds a new franchise to database.
     * @param franchise Franchise to add to database.
     * @return Created response.
     */
    @Operation(summary = "Adds a franchise")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Created",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = FranchiseDTO.class))) }),
            @ApiResponse(responseCode = "400",
                    description = "Invalid ID supplied",
                    content = { @Content( mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @PostMapping
    public ResponseEntity add(@RequestBody Franchise franchise){
        Franchise newFranchise = franchiseService.add(franchise);
        URI uri = URI.create("franchises/" + newFranchise.getId());
        return ResponseEntity.created(uri).build();
    }

    /**
     * Update operation which updates franchise with specified ID and given body.
     * @param franchiseDTO Franchise body which stores the updated properties.
     * @param id ID of franchise which should be updated.
     * @return Response with no content.
     */
    @Operation(summary = "Updates a franchise")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Franchise successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content( mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Franchise not found with supplied ID",
                    content = { @Content( mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @PutMapping(value = "/{id}")
    public ResponseEntity update(@RequestBody FranchiseDTO franchiseDTO, @PathVariable int id){
        if(franchiseDTO.getId() != id) return ResponseEntity.badRequest().build();
        franchiseService.update(franchiseMapper.franchiseDtoToFranchise(franchiseDTO));
        return ResponseEntity.noContent().build();
    }

    /**
     * Delete operation which deletes franchise by specified id.
     * @param id Franchise with specified ID to delete.
     * @return Response with no content.
     */
    @Operation(summary = "Deletes a franchise")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Franchise successfully deleted",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content( mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Franchise not found with supplied ID",
                    content = { @Content( mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @DeleteMapping(value = "/{id}")
    public ResponseEntity delete(@PathVariable int id){
        franchiseService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}