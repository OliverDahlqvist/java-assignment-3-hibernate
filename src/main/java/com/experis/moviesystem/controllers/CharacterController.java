package com.experis.moviesystem.controllers;

import com.experis.moviesystem.mappers.CharacterMapper;
import com.experis.moviesystem.models.Character;
import com.experis.moviesystem.models.dtos.CharacterDTO;
import com.experis.moviesystem.services.character.CharacterService;
import com.experis.moviesystem.util.ApiErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

@RestController
@RequestMapping(path = "api/v1/characters")
public class CharacterController {
    private final CharacterService characterService;

    private final CharacterMapper characterMapper;

    public CharacterController(CharacterService characterService, CharacterMapper characterMapper) {
        this.characterService = characterService;
        this.characterMapper = characterMapper;
    }

    /**
     * Gets character with specified ID.
     * @param id Character ID to search for.
     * @return Ok response.
     */
    @Operation(summary = "Get a character by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = CharacterDTO.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Character does not exist with supplied ID",
                    content = { @Content( mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @GetMapping(value = "/{id}")
    public ResponseEntity<CharacterDTO> findById(@PathVariable int id){
        CharacterDTO characterDTO = characterMapper.characterToCharacterDto(characterService.findById(id));
        return ResponseEntity.ok(characterDTO);
    }

    /**
     * Gets all characters from the database.
     * @return Ok response with .
     */
    @Operation(summary = "Get all characters")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = CharacterDTO.class))) }),
            @ApiResponse(responseCode = "404",
                    description = "Character does not exist with supplied ID",
                    content = { @Content( mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @GetMapping
       public ResponseEntity<Collection<CharacterDTO>> findAll(){
        Collection<CharacterDTO> charactersDto = characterMapper.charactersToCharactersDto(characterService.findAll());
        return ResponseEntity.ok(charactersDto);
    }

    /**
     * Post operation which adds a new character to database.
     * @param character Character to add to database.
     * @return Created response.
     */
    @Operation(summary = "Adds a character")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Created",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = CharacterDTO.class))) }),
            @ApiResponse(responseCode = "400",
                    description = "Invalid ID supplied",
                    content = { @Content( mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @PostMapping
        public ResponseEntity add(@RequestBody Character character){
        if(characterService.exists(character.getId())) return ResponseEntity.badRequest().build();
        Character newCharacter = characterService.add(character);
        URI uri = URI.create("characters/" + newCharacter.getId());
        return ResponseEntity.created(uri).build();
    }

    /**
     * Update operation which updates character with specified ID and given body.
     * @param characterDTO Franchise body which stores the updated properties.
     * @param id ID of character which should be updated.
     * @return Response with no content.
     */
    @Operation(summary = "Updates a character")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Character successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content( mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Character not found with supplied ID",
                    content = { @Content( mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @PutMapping(value = "/{id}")
        public ResponseEntity update(@RequestBody CharacterDTO characterDTO, @PathVariable int id){
        if(characterDTO.getId() != id) return ResponseEntity.badRequest().build();
        characterService.update(characterMapper.characterDtoToCharacter(characterDTO));
        return ResponseEntity.noContent().build();
    }

    /**
     * Delete operation which deletes character by specified id.
     * @param id Character with specified ID to delete.
     * @return Response with no content.
     */
    @Operation(summary = "Deletes a character")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Character successfully deleted",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content( mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Character not found with supplied ID",
                    content = { @Content( mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @DeleteMapping(value = "/{id}")
    public ResponseEntity delete(@PathVariable int id){
        characterService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
