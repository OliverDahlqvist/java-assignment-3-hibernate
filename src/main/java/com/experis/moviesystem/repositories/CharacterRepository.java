package com.experis.moviesystem.repositories;

import com.experis.moviesystem.models.Character;
import com.experis.moviesystem.models.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CharacterRepository extends JpaRepository<Character, Integer> {
}
