# Assignment - Java Spring Boot Rest API

In this project we created a RESTful Web-API for movies with Java Spring Boot, Hibernate, and Swagger. The API is hosted on heroku using docker. The project is deployed using a CI/CD pipeline.

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Contributors](#contributors)

## Background

This application serves a RESTful API which can be hosted as a microservice.

## Install
There is no need to install anything to access the API. Look at the `usage` section to see how to access the API endpoints.

If you want to deploy your own version of the backend API you have to install the following:
- JDK 17
- Intellij, or any other preferred development software.
- Docker

Clone this repository and start a heroku project which has the Postgres addon. You also have to copy the API key from heroku and paste it in the CI/CD settings -> Variables as `HEROKU_API_KEY` in Gitlab. Simply push to main and start the reployment from the CI/CD Pipeline page.

## Usage

### API
Feel free to use your webbrowser to access the API or use a software such as `Postman` to access the API and its responses.

1. API is hosted at experis-java-movie-api-oliver.herokuapp.com/api/v1/`query`
2. Endpoints can be accessed by replacing `query` with any of the following:
- movies
- characters
- franchises

For example: experis-java-movie-api-oliver.herokuapp.com/api/v1/movies, will return a response with a body with a collection of all the movies in the database.

3. Specify an ID behind the address to accesses individual entities with operations such as `get` and `delete`, `add`. 
For example: - `api/v1/movies/1`, will return the movie with id `1` if it exists.
- `api/v1/characters/1` with a `delete` request will delete the character widh id `1` if it exists and remove all references to the character in all other entities.

### Documentation

API documentation can be found [here.](https://experis-java-movie-api-oliver.herokuapp.com/swagger-ui/index.html)

### Local testing

To test the API you have to replace the following application properties: 
`spring.datasource.url=`
`spring.datasource.username=`
`spring.datasource.password=`

## Contributors
[@sona.rahimova](https://gitlab.com/sona.rahimova)
[@OliverDahlqvist](https://gitlab.com/OliverDahlqvist)
